;;
;; Some trivial tests of SLIB procedures.
;;

(define (assert-equal x y msg)
  (if (not (equal? x y))
      (begin
        (write `(equal? ,x ,y))
        (newline)
        (slib:error msg))))

(require 'printf)

(assert-equal #t (provided? 'printf) "provided?")

(printf "Hello, World!\n")

(assert-equal #t (string? (home-vicinity)) "home-vicinity")

(assert-equal 'unix (software-type) "software-type")

(assert-equal 42 (identity 42) "identity")

;;; This errors out on mit-scheme for some reason
;;; TODO: investigate
(if (not (eq? (scheme-implementation-type) 'MITScheme))
    (begin
      (require 'yasos)

      (assert-equal 2 (size '(1 2)) "yasos size")
      (assert-equal 2 (size '(1 . 2)) "yasos size")
      (assert-equal 5 (size "hello") "yasos size")))
      
(require 'format)

(assert-equal "hello 80 VI" (format #f "~A ~X ~@R" "hello" 128 6) "format")

(slib:exit 0)
